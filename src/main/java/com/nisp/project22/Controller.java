package com.nisp.project22;

import com.nisp.project22.dto.InvoiceDto;
import com.nisp.project22.dto.ProductDto;
import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    @FXML
    private AnchorPane mainAnchor;

    @FXML
    private TextField sellerCompany;

    @FXML
    private TextField sellerName;

    @FXML
    private TextField sellerAddress;

    @FXML
    private TextField sellerPostcode;

    @FXML
    private TextField sellerCity;

    @FXML
    private TextField buyerName;

    @FXML
    private TextField buyerAddress;

    @FXML
    private TextField buyerPostcode;

    @FXML
    private TextField buyerCity;

    @FXML
    private TextField taxRate;

    @FXML
    private Button generateButton;

    @FXML
    private Button buttonAddProduct;

    @FXML
    private SplitPane splitHorizontal;

    @FXML
    private Text emptyFieldsAlert;

    @FXML
    private Text resultMessage;

    private List<TextField> descriptions = new ArrayList<>();
    private List<TextField> quantities = new ArrayList<>();
    private List<TextField> netPrices = new ArrayList<>();

    @FXML
    void initialize() {

        addPane();
        buttonAddProduct.setOnAction(event -> {
            addPane();
        });

        taxRate.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,3}")) {
                taxRate.setText(oldValue);
            }
        });
        sellerPostcode.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,2}([\\-]\\d{0,3})?")) {
                sellerPostcode.setText(oldValue);
            }
        });
        buyerPostcode.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,2}([\\-]\\d{0,3})?")) {
                buyerPostcode.setText(oldValue);
            }
        });

        generateButton.setOnAction(event -> {

            ArrayList<ProductDto> productDtos = new ArrayList<>();

            if (areEmptyFields()) {
                emptyFieldsAlert.setVisible(true);
            } else {
                emptyFieldsAlert.setVisible(false);
                for (int i = 0; i < descriptions.size(); i++) {
                    String desc = descriptions.get(i).getText();
                    int quantity = Integer.parseInt(quantities.get(i).getText());
                    double netPrice = Double.parseDouble(netPrices.get(i).getText());
                    ProductDto productDto = new ProductDto(desc, quantity, netPrice);
                    productDtos.add(productDto);
                }

                InvoiceDto invoiceDto = new InvoiceDto(sellerCompany.getText(), sellerName.getText(),
                        sellerAddress.getText(), sellerPostcode.getText(), sellerCity.getText(),
                        buyerName.getText(), buyerAddress.getText(), buyerPostcode.getText(),
                        buyerCity.getText(), productDtos, Integer.parseInt(taxRate.getText()));

                byte[] content = NetworkController.sendData(invoiceDto);
                if (content != null) {
                    FileChooser fileChooser = new FileChooser();
                    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF File (*.pdf)", "*.pdf");
                    fileChooser.getExtensionFilters().add(extFilter);
                    fileChooser.setInitialFileName("invoice_" + System.nanoTime()/1000000);
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File file = fileChooser.showSaveDialog(mainAnchor.getScene().getWindow());

                    if (file != null) {
                        try {
                            FileUtils.writeByteArrayToFile(file, content);
                            resultMessage.setFill(Color.rgb(51, 204, 51));
                            resultMessage.setText("Wygenerowano pomyślnie!");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        resultMessage.setFill(Color.rgb(255, 0, 0));
                        resultMessage.setText("Nie udało się wybrać lokalizacji zapisu!");
                    }
                }
                else {
                    resultMessage.setFill(Color.rgb(255, 0, 0));
                    resultMessage.setText("Generowanie faktury nie powiodło się!");
                }
                resultMessage.setVisible(true);

                PauseTransition wait = new PauseTransition(Duration.seconds(4));
                wait.setOnFinished((e) -> {
                    resultMessage.setVisible(false);
                    resultMessage.setText("");
                    wait.playFromStart();
                });
                wait.play();
            }
        });
    }

    private void addPane() {
        SplitPane pane = new SplitPane();
        pane.setMaxHeight(Region.USE_PREF_SIZE);

        TextField description = new TextField();
        description.setPromptText("OPIS");
        description.setPrefWidth(394);
        description.setMaxWidth(Region.USE_COMPUTED_SIZE);
        descriptions.add(description);

        TextField quantity = new TextField();
        quantity.setPromptText("LICZBA");
        quantity.setPrefWidth(106);
        quantity.setMaxWidth(Region.USE_PREF_SIZE);
        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                quantity.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        quantities.add(quantity);

        TextField netPrice = new TextField();
        netPrice.setPromptText("CENA NETTO");
        netPrice.setPrefWidth(144);
        netPrice.setMaxWidth(Region.USE_PREF_SIZE);
        netPrice.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,7}([\\.]\\d{0,2})?")) {
                netPrice.setText(oldValue);
            }
        });
        netPrices.add(netPrice);

        pane.getItems().setAll(description, quantity, netPrice);
        splitHorizontal.getItems().add(pane);
    }

    private boolean areEmptyFields() {
        if (sellerCompany.getText().isEmpty() || sellerName.getText().isEmpty() || sellerAddress.getText().isEmpty() ||
                sellerPostcode.getText().isEmpty() || sellerCity.getText().isEmpty() || buyerName.getText().isEmpty() ||
                buyerAddress.getText().isEmpty() || buyerPostcode.getText().isEmpty() || buyerCity.getText().isEmpty() ||
                taxRate.getText().isEmpty()) {
            return true;
        }
        for (int i = 0; i < descriptions.size(); i++) {
            if (descriptions.get(i).getText().isEmpty()
                    || quantities.get(i).getText().isEmpty()
                    || netPrices.get(i).getText().isEmpty())
                return true;
        }
        return false;
    }
}
