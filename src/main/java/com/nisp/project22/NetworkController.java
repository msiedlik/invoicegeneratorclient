package com.nisp.project22;

import com.google.gson.Gson;
import com.nisp.project22.dto.InvoiceDto;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

class NetworkController {

    static byte[] sendData(InvoiceDto invoiceDto){

        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/invoice");
        final String json = new Gson().toJson(invoiceDto);

        try (CloseableHttpClient client = HttpClients.createDefault()){
            final StringEntity stringEntity = new StringEntity(json, "UTF-8");
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Content-type", "application/json;charset=UTF-8");

            final CloseableHttpResponse response = client.execute(httpPost);
            HttpEntity entity = response.getEntity();

            if(response.getStatusLine().getStatusCode() == 201){
                return EntityUtils.toByteArray(entity);
            }
            return null;

        } catch (UnsupportedEncodingException e) {
            System.out.println("Problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Problem with POST input output");
            e.printStackTrace();
        }
        return null;
    }
}
