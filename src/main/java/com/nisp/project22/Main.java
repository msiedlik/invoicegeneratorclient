package com.nisp.project22;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException{
        Parent root = FXMLLoader.load(ClassLoader.getSystemResource("invoice.fxml"));
        primaryStage.setTitle("Generator Faktur");
        primaryStage.getIcons().add(new Image("/receipt.png"));
        primaryStage.setScene(new Scene(root, 800, 690));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
