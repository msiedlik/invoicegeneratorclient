package com.nisp.project22.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class InvoiceDto implements Serializable {
    private String sellerCompany;
    private String sellerName;
    private String sellerAddress;
    private String sellerPostcode;
    private String sellerCity;
    private String buyerName;
    private String buyerAddress;
    private String buyerPostcode;
    private String buyerCity;
    private ArrayList<ProductDto> productDtos;
    private int taxRate;


    public InvoiceDto(String sellerCompany, String sellerName, String sellerAddress, String sellerPostcode, String sellerCity, String buyerName,
                      String buyerAddress, String buyerPostcode, String buyerCity, ArrayList<ProductDto> productDtos, int taxRate) {
        this.sellerCompany = sellerCompany;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
        this.sellerPostcode = sellerPostcode;
        this.sellerCity = sellerCity;
        this.buyerName = buyerName;
        this.buyerAddress = buyerAddress;
        this.buyerPostcode = buyerPostcode;
        this.buyerCity = buyerCity;
        this.productDtos = productDtos;
        this.taxRate = taxRate;
    }

    public String getSellerCompany() {
        return sellerCompany;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public String getSellerPostcode() {
        return sellerPostcode;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public String getBuyerPostcode() {
        return buyerPostcode;
    }

    public String getBuyerCity() {
        return buyerCity;
    }

    public int getTaxRate() {
        return taxRate;
    }

    public ArrayList<ProductDto> getProductDtos() {
        return productDtos;
    }
}
