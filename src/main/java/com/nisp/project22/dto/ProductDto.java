package com.nisp.project22.dto;

import java.io.Serializable;

public class ProductDto implements Serializable {

    private String description;
    private int quantity;
    private double netPrice;

    public ProductDto(String description, int quantity, double netPrice) {
        this.description = description;
        this.quantity = quantity;
        this.netPrice = netPrice;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

}
